<?php

namespace GatherContentStreamingClient\DetailQueryBuilder;

use GuzzleHttp\Client as GuzzleClient;

class ItemQueryBuilder implements DetailQueryBuilderInterface
{
    private $guzzle;

    const ENDPOINT = '/items/:id';

    public function __construct(GuzzleClient $guzzle)
    {
        $this->guzzle = $guzzle;
    }


    public function get(String $id): Array
    {
        $response = $this->guzzle->get(
            str_replace(':id', $id, self::ENDPOINT)
        );

        $rawData = json_decode($response->getBody()->getContents(), true);

        return [
            'template_id' => $rawData['data']['template_id'],
            'title' => $rawData['data']['name'],
            'content_sections' =>
                self::normalizeReturn($rawData['data']['config'])
        ];
    }

    static private function normalizeReturn(Array $rawSections): Array
    {
        foreach ($rawSections as $rawSection) {
            foreach ($rawSection['elements'] as $rawElement) {
                $normalized[$rawSection['label']][$rawElement['label']] =
                    $rawElement['value'];
            }
        }

        return $normalized;
    }

}
