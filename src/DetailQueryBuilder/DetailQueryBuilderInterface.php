<?php

namespace GatherContentStreamingClient\DetailQueryBuilder;

interface DetailQueryBuilderInterface
{
    public function get(String $id): Array;
}
