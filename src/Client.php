<?php

namespace GatherContentStreamingClient;

use GatherContentStreamingClient\CollectionQueryBuilder\{
    ItemsQueryBuilder
};
use GatherContentStreamingClient\DetailQueryBuilder\{
    ItemQueryBuilder
};
use GuzzleHttp\Client as GuzzleClient;

class Client
{
    private $guzzle;

    public function __construct(String $user, String $apiKey)
    {
        $this->guzzle = new GuzzleClient([
            'base_uri' => 'https://api.gathercontent.com',
            'auth' => [
                $user,
                $apiKey
            ],
        ]);
    }

    public function createItemsQuery(String $projectId): ItemsQueryBuilder
    {
        return new ItemsQueryBuilder(
            $projectId,
            $this->guzzle
        );
    }

    public function createItemQuery(): ItemQueryBuilder
    {
        return new ItemQueryBuilder(
            $this->guzzle
        );
    }
}
