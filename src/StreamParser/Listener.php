<?php

/**
 *
 * This file is slightly modified from its source in
 * https://github.com/MAXakaWIZARD/JsonCollectionParser for compatibility
 * with the latest version of JsonStreamingParser and a slightly different
 * JSON result format. Those modifications are subject to this project's
 * copyright and licensing. Original copyright and license reproduced here:
 *
 * Copyright (c) 2015 Max Grigorian

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * */

namespace GatherContentStreamingClient\StreamParser;

use \JsonStreamingParser\Listener\ListenerInterface;

class Listener implements ListenerInterface
{
    /**
     * @var array
     */
    protected $stack;

    /**
     * @var string
     */
    protected $key;

    /**
     * @var array
     */
    protected $keys;

    /**
     * @var int
     */
    protected $level;

    /**
     * @var int
     */
    protected $objectLevel;

    /**
     * @var array
     */
    protected $objectKeys;

    /**
     * @var callback|callable
     */
    protected $callback;

    /**
     * @var callback|callable
     */
    protected $endCallback;

    /**
     * @var bool
     */
    protected $assoc;

    /**
     * @param callback|callable $callback callback for parsed collection item
     * @param bool $assoc When true, returned objects will be converted into associative arrays
     */
    public function __construct(
        Callable $itemCallback,
        Callable $endCallback,
        $assoc = true
    ) {
        $this->callback = $itemCallback;
        $this->endCallback = $endCallback;
        $this->assoc = $assoc;
    }

    public function startDocument(): void
    {
        $this->stack = [];
        $this->key = null;
        $this->keys = [];
        $this->objectLevel = 0;
        $this->level = 0;
        $this->objectKeys = [];
    }

    public function endDocument(): void
    {
        $this->stack = [];
        $this->keys = [];
        call_user_func($this->endCallback);
    }

    public function startObject(): void
    {
        $this->objectLevel++;
        $this->startCommon();
    }

    public function endObject(): void
    {
        $this->endCommon();
        $this->objectLevel--;
        if ($this->objectLevel === 1) {
            $obj = $this->stack[1][0];
            array_shift($this->stack[1]);
            call_user_func($this->callback, $obj);
        }
    }

    public function startArray(): void
    {
        $this->startCommon();
    }

    public function startCommon(): void
    {
        $this->level++;
        $this->objectKeys[$this->level] = ($this->key) ? $this->key : null;
        $this->key = null;
        array_push($this->stack, []);
    }

    public function endArray(): void
    {
        $this->endCommon(false);
    }

    public function endCommon($isObject = true): void
    {
        $obj = array_pop($this->stack);
        if ($isObject && !$this->assoc) {
            $obj = (object)$obj;
        }
        if (!empty($this->stack)) {
            $parentObj = array_pop($this->stack);
            if ($this->objectKeys[$this->level]) {
                $parentObj[$this->objectKeys[$this->level]] = $obj;
            } else {
                array_push($parentObj, $obj);
            }
            array_push($this->stack, $parentObj);
        }
        $this->level--;
    }

    /**
     * @param string $key
     */
    public function key(String $key): void
    {
        $this->key = $key;
    }

    /**
     * @param mixed $value
     */
    public function value($value): void
    {
        $obj = array_pop($this->stack);
        if ($this->key) {
            $obj[$this->key] = $value;
            $this->key = null;
        } else {
            array_push($obj, $value);
        }
        array_push($this->stack, $obj);
    }

    /**
     * @param string $whitespace
     */
    public function whitespace(String $whitespace): void
    {
    }

}
