<?php

namespace GatherContentStreamingClient\CollectionQueryBuilder;

use GuzzleHttp\Promise\Promise;

interface CollectionQueryBuilderInterface
{
    public function addFilter(Callable $filter): CollectionQueryBuilderInterface;

    public function get(Int $numberOfItems, Int $offset, Bool $async);

    public function each(Callable $onItem, Callable $onEnd): void;

}
