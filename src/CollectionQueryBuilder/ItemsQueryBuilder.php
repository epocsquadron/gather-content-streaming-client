<?php

namespace GatherContentStreamingClient\CollectionQueryBuilder;

use GatherContentStreamingClient\Client;
use GatherContentStreamingClient\StreamParser\Listener;
use GatherContentStreamingClient\Support\Exceptions\RequiredScopeException;
use JsonStreamingParser\Parser;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Promise\Promise;
use GuzzleHttp\Psr7\{
    Response,
    StreamWrapper
};


class ItemsQueryBuilder implements CollectionQueryBuilderInterface
{
    private $user = '';
    private $apiKey = '';
    private $guzzle;

    private $projectId;
    private $filters = [];
    private $results = [];
    private $index = 0;

    const ENDPOINT = '/items';

    public function __construct(String $projectId, GuzzleClient $guzzle)
    {
        $this->guzzle = $guzzle;
        $this->projectId = $projectId;

        if (!$this->projectId) {
            throw new RequiredScopeException('Items can only be queried if the project is identified.');
        }

    }

    /**
     * Applies a scope so only items within a specific folder are processed by each
     *
     * @param String $folderUuid Uuid returned by the Gather Content API for the folder
     * @return CollectionQueryBuilderInterface This object for chaining with more filters/scopes
     */
    public function folder(String $folderUuid): CollectionQueryBuilderInterface
    {
        return $this->addFilter(function ($item) use ($folderUuid) {
            if (
                isset($item['folder_uuid'])
                && $folderUuid == $item['folder_uuid']
            ) {
                return $item;
            }
            return false;
        });
    }

	/**
	 * Adds a filter to stack of those applied during streaming of results
	 *
     * The callback provided should take one argument, an array
     * describing the current item. Return either the item to continue
     * in the stack of filters, or false to prevent the item from being
     * further processed and skip to the next one.
     *
     * You may modify the item before returning it, but keep in mind
     * to check against prior modification. If, for example, you apply
     * a filter that removes the folder_uuid, then the filter must be
     * applied after the `folder` method is called, or that filter will
     * break.
     *
	 * @param Callable $filter The filter
	 * @return CollectionQueryBuilderInterface This object for chaining
	 */
    public function addFilter(Callable $filter): CollectionQueryBuilderInterface
    {
        // Add a callback to the array of filters which will be composed
        // during the impending call to `each`.
        $this->filters[] = $filter;

        return $this;
    }

    /**
     * Collect a certain number of items and return at once
     *
     * If there are fewer than the requested items, the promise
     * will just resolve with as many items as there are. Returns
     * an empty array if none at all.
     *
     * @param Int $numberOfItems Number of items to take
     * @param Int $offset Optional offset, zero-based
     * @param Bool $async Whether to unwrap the underlying promise
     * @return Array|Promise An A+ compatible promise containing results
     */
    public function get(Int $numberOfItems = null, Int $offset = 0,  Bool $async = false)
    {
        $promise = new Promise();


        $saveItem = function ($item) use ($numberOfItems, $offset, $promise) {
            if ($this->index >= $offset) {
                $this->results[] = $item;
            }

            if (
                // We have reached the maximum results requested
                $numberOfItems
                && count($this->results) == $numberOfItems
            ) {
                $promise->resolve($this->results);
            }

            $this->index++;
        };

        // If by some combination of maximum and offset we end up with
        // a result set that is less than the maximum at the end, then
        // we haven't resolved yet, so do it at the end.
        $resolveIfShort = function () use ($numberOfItems, $promise) {
            if (count($this->results) < $numberOfItems) {
                $promise->resolve();
            }
        };

        // We let our already well behaved asynchronous method for
        //looping through results do the heavy lifting and just record
        //results for resolution at the end.
        $this->each(
            $this->combineFilterAndCallbackStack($saveItem),
            $resolveIfShort
        );

        // Default is to return the actual results, assuming that the
        // user will have used the `each` method if they wanted to treat
        // it asynchronously. However, there's no reason not to provide
        // the ability for the user to request the internal promise
        // instead.
        return $async ? $promise : $promise->wait();
    }

    /**
     * Apply a callback to every returned Item as it streams in
     *
     * @param Callable $onItem Callback to apply on every item, passed the item as an array
     * @param Callable $onEnd Optional callback to apply when end of stream is reached
     */
    public function each(Callable $onItem, Callable $onEnd = null): void
    {
        // Listener class needs an onEnd even though we've made it
        // optional for this method, so provide an empty closure to
        // satisfy it.
        $onEnd = $onEnd ?? function () {};

        // Construct observer object for parser to notify to
        $listener = new Listener(
            $this->combineFilterAndCallbackStack($onItem),
            $onEnd
        );

        // Get a stream of the raw json from the endpoint
        $response = $this->guzzle->get(
            self::ENDPOINT,
            [
                'stream' => true,
                'query' => ['project_id' => $this->projectId],
            ]
        );

        // Construct stream-consuming json parser
        $parser = new Parser(
            StreamWrapper::getResource($response->getBody()),
            $listener
        );

        $parser->parse();
    }

    /**
     * Composes callback and the callables in the filters array into a single closure
     *
     * @param Callable $callback Method to apply after filters
     * @return Closure New function that calls all of the filters followed by the callback
     */
    private function combineFilterAndCallbackStack(Callable $callback): \Closure
    {
        return function ($item) use ($callback) {
            foreach ($this->filters as $filter) {
                // Filters are allowed to trim or modify the result, so save
                // the result for the next pass and eventually to be added
                // to the results array
                $item = $filter($item);

                // In case where a filter sets the item to false, it means
                // we shouldn't add to the final results.
                if ($item === false) {
                    return;
                }
            }

            $callback($item);
        };
    }

}
