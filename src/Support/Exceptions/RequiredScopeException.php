<?php

namespace GatherContentStreamingClient\Support\Exceptions;

class RequiredScopeException extends \Exception {}
